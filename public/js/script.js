{
    // calculates cost per day of given entry and updates the corresponding preview element on it
    function updatePreview(entry){
        const cost = entry.getElementsByClassName('cost')[0];
        const frequencyAmount = entry.getElementsByClassName('frequencyAmount')[0];
        const frequencyType = entry.getElementsByClassName('frequencyType')[0];

        entry.getElementsByClassName('preview')[0].innerHTML = ((cost.value / (frequencyAmount.value * frequencyType.value)).toFixed(2) + " per day");
    }

    // returns all entries as an array
    function allSavedEntries(){
        const arr = [];
        const savedEntries = document.getElementById('savedEntries');

        if (savedEntries.children.length != 0){
            for (let i = 0; i < savedEntries.children.length; i++){
                arr.push(savedEntries.children[i].firstElementChild.value);
            }
        }
        return arr;
    }
    
    // adds entry to savedEntries element and removes old entry with the same name if that's the case
    function addEntry(){
        const savedEntries = document.getElementById('savedEntries');
        const input = document.getElementById('input').firstElementChild.value;
        const duplicate = allSavedEntries().indexOf(input);
        const newEntry = document.getElementById('input').cloneNode(true);

        if (duplicate != -1)
            savedEntries.children[duplicate].remove();
        newEntry.removeAttribute('id');
        updatePreview(newEntry);
        newEntry.getElementsByClassName('frequencyType')[0].value = document.getElementById('input').getElementsByClassName('frequencyType')[0].value
        document.getElementById('savedEntries').appendChild(newEntry);
    }

    // events
    document.getElementById("save").addEventListener("click", addEntry);
    document.addEventListener('change', e => {
        if (e.target.matches('.cost, .frequencyAmount, .frequencyType'))
            updatePreview(e.target.parentElement);
    })
}